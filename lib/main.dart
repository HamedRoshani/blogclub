import 'package:blogclub/article.dart';
import 'package:blogclub/carousel/carousel_slider.dart';
import 'package:blogclub/gen/assets.gen.dart';
import 'package:blogclub/home.dart';
import 'package:blogclub/profile.dart';
import 'package:blogclub/splash.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:blogclub/data.dart';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.white,
      statusBarIconBrightness: Brightness.dark,
      systemNavigationBarColor: Colors.white,
      systemNavigationBarIconBrightness: Brightness.dark));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  static const defualtFont = 'Avenir';
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final primaryTextColor = Color(0xff0D253C);
    final secondoryTextColor = Color(0xff2D4379);
    final primarycolor = Color(0xff386BED);
    const defualtFont = 'Avenir';

    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          colorScheme: ColorScheme.light(
            primary: primarycolor,
            onPrimary: Colors.white,
            onSurface: primaryTextColor,
            background: Color(0xffE6EAF1),
            surface: Colors.white,
            onBackground: primaryTextColor,
          ),
          scaffoldBackgroundColor: Color(0xffE6EAF1),
          textButtonTheme: TextButtonThemeData(
            style: ButtonStyle(
              textStyle: MaterialStateProperty.all(
                TextStyle(
                  color: primarycolor,
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  fontFamily: defualtFont,
                ),
              ),
            ),
          ),
          textTheme: TextTheme(
            subtitle1: TextStyle(
              fontWeight: FontWeight.w200,
              fontFamily: defualtFont,
              color: secondoryTextColor,
              fontSize: 18,
            ),
            caption: TextStyle(
              fontFamily: defualtFont,
              fontWeight: FontWeight.w700,
              color: Color(0xff7B8BB2),
              fontSize: 10,
            ),
            subtitle2: TextStyle(
              fontWeight: FontWeight.w400,
              fontFamily: defualtFont,
              color: primaryTextColor,
              fontSize: 14,
            ),
            headline5: TextStyle(
              fontFamily: defualtFont,
              fontSize: 20,
              color: primaryTextColor,
              fontWeight: FontWeight.w700,
            ),
            headline6: TextStyle(
              fontFamily: defualtFont,
              fontWeight: FontWeight.bold,
              fontSize: 18,
              color: primaryTextColor,
            ),
            headline4: TextStyle(
              fontFamily: defualtFont,
              fontSize: 24,
              color: primaryTextColor,
              fontWeight: FontWeight.w700,
            ),
            bodyText2: TextStyle(
              fontFamily: defualtFont,
              color: secondoryTextColor,
              fontSize: 12,
            ),
            bodyText1: TextStyle(
              fontFamily: defualtFont,
              color: primaryTextColor,
              fontSize: 14,
            ),
          ),
          appBarTheme: AppBarTheme(
            backgroundColor: Colors.white,
            foregroundColor: primaryTextColor,
            titleSpacing: 32,
          ),
          snackBarTheme: SnackBarThemeData(
            backgroundColor: primarycolor,
          )),
      // home: Stack(
      //   children: [
      //     Positioned.fill(child: HomeScreen()),
      //     Positioned(bottom: 0, right: 0, left: 0, child: BottomNavigation()),
      //   ],
      // ),
      home: SplashScreen(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class Mainscreen extends StatefulWidget {
  const Mainscreen({Key? key}) : super(key: key);

  @override
  State<Mainscreen> createState() => _MainscreenState();
}

const int homeindex = 0;
const int articleindex = 1;
const int searchindex = 2;
const int menuindex = 3;
const double bottomNavigationHeigth = 65;

class _MainscreenState extends State<Mainscreen> {
  int selectedScreenindex = homeindex;

  final List<int> _history = [];

  GlobalKey<NavigatorState> homekey = GlobalKey();
  GlobalKey<NavigatorState> articlekey = GlobalKey();
  GlobalKey<NavigatorState> searchkey = GlobalKey();
  GlobalKey<NavigatorState> menukey = GlobalKey();

  late final map = {
    homeindex: homekey,
    articleindex: articlekey,
    searchindex: searchkey,
    menuindex: menukey,
  };

  Future<bool> onwillpop() async {
    final NavigatorState currentSelectedTapNavigationState =
        map[selectedScreenindex]!.currentState!;
    if (currentSelectedTapNavigationState.canPop()) {
      currentSelectedTapNavigationState.pop();
      return false;
    } else if (_history.isNotEmpty) {
      setState(() {
        selectedScreenindex = _history.last;
        _history.removeLast();
      });
      return false;
    }

    return true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onwillpop,
      child: Scaffold(
        body: Stack(
          children: [
            Positioned.fill(
              bottom: bottomNavigationHeigth,
              child: IndexedStack(
                index: selectedScreenindex,
                children: [
                  _Navigator(homekey, homeindex, HomeScreen()),
                  _Navigator(articlekey, articleindex, ArticleScreen()),
                  _Navigator(searchkey, searchindex, Searchscreen()),
                  _Navigator(menukey, menuindex, Profilescreen()),
                ],
              ),
            ),
            Positioned(
              bottom: 0,
              right: 0,
              left: 0,
              child: BottomNavigation(
                selectedindex: selectedScreenindex,
                ontap: (int index) {
                  setState(() {
                    _history.remove(selectedScreenindex);
                    _history.add(selectedScreenindex);
                    selectedScreenindex = index;
                  });
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _Navigator(GlobalKey key, int index, Widget child) {
    return key.currentState == null && selectedScreenindex != index
        ? Container()
        : Navigator(
            key: key,
            onGenerateRoute: (settings) => MaterialPageRoute(
              builder: (context) => Offstage(
                  offstage: selectedScreenindex != index, child: child),
            ),
          );
  }
}

class Searchscreen extends StatelessWidget {
  const Searchscreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(
          'search screen ',
          style: Theme.of(context).textTheme.headline4,
        ),
      ),
    );
  }
}

class BottomNavigation extends StatelessWidget {
  final Function(int index) ontap;
  final int selectedindex;

  const BottomNavigation(
      {Key? key, required this.ontap, required this.selectedindex})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 85,
        child: Stack(
          children: [
            Positioned(
              left: 0,
              right: 0,
              bottom: 0,
              child: Container(
                height: 65,
                decoration: BoxDecoration(color: Colors.white, boxShadow: [
                  BoxShadow(
                      blurRadius: 20, color: Color(0xff9B8487).withOpacity(0.3))
                ]),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    BottomNavigationItem(
                      iconFilename: 'Home.png',
                      activeIconfilename: 'Home.png',
                      title: "Home",
                      ontap: () {
                        ontap(homeindex);
                      },
                      isActive: selectedindex == homeindex,
                    ),
                    BottomNavigationItem(
                      iconFilename: 'Articles.png',
                      activeIconfilename: 'Articles.png',
                      title: "Articles",
                      ontap: () {
                        ontap(articleindex);
                      },
                      isActive: selectedindex == articleindex,
                    ),
                    Expanded(child: Container()),
                    BottomNavigationItem(
                      iconFilename: 'Search.png',
                      activeIconfilename: 'Search.png',
                      title: "Search",
                      ontap: () {
                        ontap(searchindex);
                      },
                      isActive: selectedindex == searchindex,
                    ),
                    BottomNavigationItem(
                      iconFilename: 'Menu.png',
                      activeIconfilename: 'Menu.png',
                      title: "Menu",
                      ontap: () {
                        ontap(menuindex);
                      },
                      isActive: selectedindex == menuindex,
                    ),
                  ],
                ),
              ),
            ),
            Center(
              child: Container(
                width: 65,
                height: 85,
                alignment: Alignment.topCenter,
                child: Container(
                  height: bottomNavigationHeigth,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(32.5),
                      color: Color(0xff376AED),
                      border: Border.all(color: Colors.white, width: 4)),
                  child: Assets.img.icons.plus.image(),
                ),
              ),
            )
          ],
        ));
  }
}

class BottomNavigationItem extends StatelessWidget {
  final String iconFilename;
  final String activeIconfilename;
  final String title;
  final bool isActive;
  final Function() ontap;
  const BottomNavigationItem(
      {Key? key,
      required this.iconFilename,
      required this.activeIconfilename,
      required this.title,
      required this.ontap,
      required this.isActive})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    return Expanded(
      child: InkWell(
        onTap: ontap,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
                'assets/img/icons/${isActive ? activeIconfilename : iconFilename}'),
            SizedBox(
              height: 4,
            ),
            Text(
              title,
              style: themeData.textTheme.caption!.apply(
                  color: isActive
                      ? themeData.colorScheme.primary
                      : themeData.textTheme.caption!.color),
            ),
          ],
        ),
      ),
    );
  }
}
